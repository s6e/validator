<?php

namespace S6e\Validator\Tests;

use PHPUnit\Framework\TestCase;
use S6e\Validator\Rules\IsOneStringOf;

class IsOneStringOfRuleTest extends TestCase
{
    public function testSprintfOnMessageAndParamsWillReturnProperMessage()
    {
        $rule = new IsOneStringOf("", ["1", "2", "5"]);

        $this->assertEquals("One string of 1, 2, 5 is required.", sprintf($rule->getMessage(), ...$rule->getMessageParams()));
    }
}
