<?php

namespace S6e\Validator\Tests;

use S6e\Validator\Rules\Counts;
use PHPUnit\Framework\TestCase;
use S6e\Validator\Rules\IsBoolean;
use S6e\Validator\Rules\IsInt;

class IsBooleanRuleTest extends TestCase
{
    public function testCheckWillReturnTrueIfTrueString()
    {
        $value = 'true';
        $rule = new IsBoolean("");

        $this->assertTrue($rule->check($value));
    }
    public function testCheckIsCaseInsensitiveString()
    {
        $value = 'tRuE';
        $rule = new IsBoolean("");

        $this->assertTrue($rule->check($value));
    }
    public function testCheckWillReturnTrueIfFalseString()
    {
        $value = 'false';
        $rule = new IsBoolean("");

        $this->assertTrue($rule->check($value));
    }
    public function testCheckIsCaseInsensitiveFalseString()
    {
        $value = 'FalSe';
        $rule = new IsBoolean("");

        $this->assertTrue($rule->check($value));
    }

    public function testCheckWillReturnFalseIfString ()
    {
        $value = 'asds';
        $rule = new IsInt("");

        $this->assertFalse($rule->check($value));
    }
}
