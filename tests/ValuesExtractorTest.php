<?php

namespace S6e\Validator\Tests;

use PHPUnit\Framework\TestCase;
use S6e\Validator\ValuesExtractor;
use stdClass;

class ValuesExtractorTest extends TestCase
{
    const TEST_VALUE_1 = "testValue1";
    const TEST_VALUE_2 = 2;

    const ARRAY_KEY = "key1";
    const PROPERTY1_NAME = "property1";
    const PROPERTY2_NAME = "property2";
    const ARRAY_PROPERTY_NAME = "array_property";

    public function testNullPathReturnsSame()
    {
        $value = new stdClass();
        $extracted = ValuesExtractor::extract(null, $value);

        $this->assertSame($value, $extracted[0]->getValue());
        $this->assertNull($extracted[0]->getPath());
    }

    public function testPathReturnsAssocArrayElement()
    {
        $value = [ self::ARRAY_KEY => self::TEST_VALUE_1];
        $extracted = ValuesExtractor::extract(self::ARRAY_KEY, $value);

        $this->assertSame(self::TEST_VALUE_1, $extracted[0]->getValue());
        $this->assertEquals(self::ARRAY_KEY, $extracted[0]->getPath());
    }

    public function testEachPathReturnsAllArrayElements()
    {
        $value = [ self::ARRAY_KEY => self::TEST_VALUE_1, self::TEST_VALUE_2];
        $extracted = ValuesExtractor::extract(":each", $value);

        $this->assertSame(self::TEST_VALUE_1, $extracted[0]->getValue());
        $this->assertEquals(":each.".self::ARRAY_KEY, $extracted[0]->getPath());

        $this->assertSame(self::TEST_VALUE_2, $extracted[1]->getValue());
        $this->assertEquals(":each.0", $extracted[1]->getPath());
    }

    public function testPathReturnsPropertyValue()
    {
        $value = new TestData();
        $value->setProperty1(self::TEST_VALUE_1);
        $extracted = ValuesExtractor::extract(self::PROPERTY1_NAME, $value);

        $this->assertSame(self::TEST_VALUE_1, $extracted[0]->getValue());
        $this->assertEquals(self::PROPERTY1_NAME, $extracted[0]->getPath());
    }

    public function testDeepPathReturnsPropertyValue()
    {
        $value = new TestData();
        $value->setProperty1(new TestData());
        $value->getProperty1()->setProperty2(new TestData());
        $value->getProperty1()->getProperty2()->setArrayProperty([
            "what",
            "if"
        ]);
        $extracted = ValuesExtractor::extract(
            implode(".", [self::PROPERTY1_NAME, self::PROPERTY2_NAME, self::ARRAY_PROPERTY_NAME, ":each"]),
            $value
        );

        $this->assertSame("what", $extracted[0]->getValue());
        $this->assertEquals(implode(".", [self::PROPERTY1_NAME, self::PROPERTY2_NAME, self::ARRAY_PROPERTY_NAME, ":each.0"]), $extracted[0]->getPath());
        $this->assertSame("if", $extracted[1]->getValue());
        $this->assertEquals(implode(".", [self::PROPERTY1_NAME, self::PROPERTY2_NAME, self::ARRAY_PROPERTY_NAME, ":each.1"]), $extracted[1]->getPath());
    }

    public function testReturnsNullIfPathNotExists() {
        $value = [ ];
        $extracted = ValuesExtractor::extract("part1.:each.part2", $value);

        $this->assertNull($extracted[0]->getValue());
        $this->assertEquals("part1.:each.part2", $extracted[0]->getPath());
        $this->assertFalse($extracted[0]->isPathExists());
    }

    public function testEachPathReturnsAllArrayKeys()
    {
        $value = [ self::ARRAY_KEY => self::TEST_VALUE_1, self::TEST_VALUE_2];
        $extracted = ValuesExtractor::extract(":keys", $value);

        $this->assertSame(self::ARRAY_KEY, $extracted[0]->getValue());
        $this->assertEquals(":keys.0", $extracted[0]->getPath());

        $this->assertSame(0, $extracted[1]->getValue());
        $this->assertEquals(":keys.1", $extracted[1]->getPath());
    }
}
