<?php

namespace S6e\Validator\Tests;

use S6e\Validator\Rules\Counts;
use PHPUnit\Framework\TestCase;

class CountsRuleTest extends TestCase
{
    public function testCheckWillReturnTrueIfArrayCountMatch ()
    {
        $value = [1, 2, 3];
        $rule = new Counts("", 3);

        $this->assertTrue($rule->check($value));
    }

    public function testCheckWillReturnTrueIfArrayCountDoesntMatch()
    {
        $value = [1, 2, 3];
        $rule = new Counts("", 2);

        $this->assertFalse($rule->check($value));
    }

    public function testCheckWillReturnTrueIfNotArray()
    {
        $value = "a";
        $rule = new Counts("", 2);

        $this->assertFalse($rule->check($value));
    }
}
