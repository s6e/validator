<?php

namespace S6e\Validator\Tests;

class TestData
{
    private mixed $property1;
    private mixed $property2;
    private array $arrayProperty;

    public function getProperty1(): mixed {
        return $this->property1;
    }

    /**
     * @param mixed $value
     * @return TestData
     */
    public function setProperty1(mixed $value): TestData
    {
        $this->property1 = $value;
        return $this;
    }

    public function getProperty2(): mixed {
        return $this->property2;
    }

    /**
     * @param mixed $value
     * @return TestData
     */
    public function setProperty2(mixed $value): TestData
    {
        $this->property2 = $value;
        return $this;
    }

    public function getArrayProperty(): array {
        return $this->arrayProperty;
    }

    /**
     * @param array $value
     * @return TestData
     */
    public function setArrayProperty(array $value): TestData
    {
        $this->arrayProperty = $value;
        return $this;
    }
}
