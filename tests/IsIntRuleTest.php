<?php

namespace S6e\Validator\Tests;

use S6e\Validator\Rules\Counts;
use PHPUnit\Framework\TestCase;
use S6e\Validator\Rules\IsInt;

class IsIntRuleTest extends TestCase
{
    public function testCheckWillReturnTrueIfIntString ()
    {
        $value = '1';
        $rule = new IsInt("");

        $this->assertTrue($rule->check($value));
    }

    public function testCheckWillReturnTrueIfInt ()
    {
        $value = 5;
        $rule = new IsInt("");

        $this->assertTrue($rule->check($value));
    }

    public function testCheckWillReturnFalseIfString ()
    {
        $value = 'asds';
        $rule = new IsInt("");

        $this->assertFalse($rule->check($value));
    }

    public function testCheckWillReturnFalseIfDecimalString()
    {
        $value = '1.1';
        $rule = new IsInt("");

        $this->assertFalse($rule->check($value));
    }
}
