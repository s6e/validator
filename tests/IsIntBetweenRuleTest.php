<?php

namespace S6e\Validator\Tests;

use PHPUnit\Framework\TestCase;
use S6e\Validator\Rules\IsIntBetween;

class IsIntBetweenRuleTest extends TestCase
{
    public function testSprintfOnMessageAndParamsWillReturnProperMessage()
    {
        $rule = new IsIntBetween("", 2, 5);

        $this->assertEquals("Integer value in range from 2 to 5 is required.", sprintf($rule->getMessage(), ...$rule->getMessageParams()));
    }
}
