<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsEmail extends BaseRule
{
    public function __construct(?string $path, string $type = "emailRequired", string $message = "Email required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_string($value) && !empty($value) && filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}
