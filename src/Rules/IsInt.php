<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsInt extends BaseRule
{
    public function __construct(?string $path, string $type = "intRequired", string $message = "Integer value required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_int(filter_var($value, FILTER_VALIDATE_INT));
    }
}
