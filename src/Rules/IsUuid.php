<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsUuid extends BaseRule
{
    const UUID_PATTERN = '^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$';

    public function __construct(?string $path, string $type = "uuidRequired", string $message = "UUID required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_string($value) && !empty($value) && preg_match("/" . self::UUID_PATTERN . "/i", $value);
    }
}
