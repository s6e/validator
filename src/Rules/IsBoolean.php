<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsBoolean extends BaseRule
{
    public function __construct(?string $path, string $type = "booleanRequired", string $message = "Boolean required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_bool(filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }
}
