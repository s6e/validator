<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsNonEmptyString extends BaseRule
{
    public function __construct(?string $path, string $type = "nonEmptyStringRequired", string $message = "Non empty string required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_string($value) === true && !empty($value);
    }
}
