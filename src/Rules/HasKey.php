<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class HasKey extends BaseRule
{
    private mixed $key;

    public function __construct(?string $path, mixed $key, string $type = "arrayKeyRequired", string $message = "Array Key %s is required.")
    {
        parent::__construct($path, $type, $message, $key);
        $this->key = $key;
    }

    public function check($value): bool
    {
        return is_array($value) && array_key_exists($this->key, $value);
    }
}
