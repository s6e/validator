<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsNull extends BaseRule
{
    public function __construct(?string $path, string $type = "nullRequired", string $message = "Null required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return ($value === null);
    }
}
