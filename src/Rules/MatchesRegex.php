<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class MatchesRegex extends BaseRule
{
    private string $regex;

    public function __construct(?string $path, string $regex, string $type = "regexMatchRequired", string $message = "Value matching regex %s is required.")
    {
        parent::__construct($path, $type, $message, $regex);
        $this->regex = $regex;
    }

    public function check($value): bool
    {
        return is_string($value) && preg_match($this->regex, $value);
    }
}
