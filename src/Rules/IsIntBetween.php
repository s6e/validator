<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsIntBetween extends BaseRule
{
    private int $from;
    private int $to;

    public function __construct(?string $path, int $from, int $to, string $type = "intInRangeRequired", string $message = "Integer value in range from %s to %s is required.")
    {
        parent::__construct($path, $type, $message, $from, $to);
        $this->from = $from;
        $this->to = $to;
    }

    public function check($value): bool
    {
        return is_int($value) && $value >= $this->from && $value <= $this->to;
    }
}
