<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsSubsetOf extends BaseRule
{
    private array $list;

    public function __construct(?string $path, array $list, string $type = "subsetOfRequired", string $message = "Subset of [%s] required.")
    {
        parent::__construct($path, $type, $message, $list);
        $this->list = $list;
    }


    public function check($value): bool
    {
        return is_array($value) && count($value) > 0 && count(array_diff($value, $this->list)) == 0;
    }
}
