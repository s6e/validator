<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsString extends BaseRule
{
    public function __construct(?string $path, string $type = "stringRequired", string $message = "String required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_string($value) === true;
    }
}
