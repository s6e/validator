<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class Counts extends BaseRule
{
    private int $count;

    public function __construct(?string $path, int $count, string $type = "countRequired", string $message = "Count %s required.")
    {
        parent::__construct($path, $type, $message, $count);
        $this->count = $count;
    }

    public function check($value): bool
    {
        return is_array($value) && count($value) == $this->count;
    }
}
