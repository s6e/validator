<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class HasMinLength extends BaseRule
{
    private int $minLength;

    public function __construct(?string $path, int $minLength, string $type = "minLengthRequired", string $message = "Required string maximum length %s.")
    {
        parent::__construct($path, $type, $message, $minLength);
        $this->minLength = $minLength;
    }

    public function check($value): bool
    {
        return is_string($value) && strlen($value) >= $this->minLength;
    }
}
