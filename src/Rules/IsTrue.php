<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsTrue extends BaseRule
{
    public function __construct(?string $path, string $type = "trueRequired", string $message = "True required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return $value === true;
    }
}
