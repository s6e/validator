<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsFalse extends BaseRule
{
    public function __construct(?string $path, string $type = "falseRequired", string $message = "False required")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return $value === false;
    }
}
