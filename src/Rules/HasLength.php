<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class HasLength extends BaseRule
{
    private int $length;

    public function __construct(?string $path, int $length, string $type = "lengthRequired", string $message = "Required string length %s.")
    {
        parent::__construct($path, $type, $message, $length);
        $this->length = $length;
    }

    public function check($value): bool
    {
        return is_string($value) && strlen($value) == $this->length;
    }
}
