<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsIntInRange extends BaseRule
{
    private array $range;

    public function __construct(?string $path, array $range, string $type = "intRequired", string $message = "Integer value in range %s required.")
    {
        parent::__construct($path, $type, $message, $range);
        $this->range = $range;
    }

    public function check($value): bool
    {
        return is_int($value) && in_array($value, $this->range, true);
    }
}
