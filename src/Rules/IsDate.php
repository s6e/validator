<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsDate extends BaseRule
{
    private string $format;

    public function __construct(?string $path, string $format, string $type = "dateRequired", string $message = "Date required in format %s.")
    {
        parent::__construct($path, $type, $message, $format);
        $this->format = $format;
    }

    public function check($value): bool
    {
        $dateTime = \DateTime::createFromFormat($this->format, $value);

        return $dateTime && $dateTime->format($this->format) == $value;
    }
}
