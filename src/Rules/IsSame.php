<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsSame extends BaseRule
{
    private mixed $reference;

    public function __construct(?string $path, mixed $reference, string $type = "sameRequired", string $message = "Value same as %s required.")
    {
        parent::__construct($path, $type, $message, $reference);
        $this->reference = $reference;
    }

    public function check($value): bool
    {
        return $value === $this->reference;
    }
}
