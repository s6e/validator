<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class HasMaxLength extends BaseRule
{
    private int $maxLength;

    public function __construct(?string $path, int $maxLength, string $type = "maxLengthRequired", string $message = "Required string maximum length %s.")
    {
        parent::__construct($path, $type, $message, $maxLength);
        $this->maxLength = $maxLength;
    }

    public function check($value): bool
    {
        return is_string($value) && strlen($value) <= $this->maxLength;
    }
}
