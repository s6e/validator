<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsArray extends BaseRule
{
    public function __construct(?string $path, string $type = "arrayRequired", string $message = "Array required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_array($value);
    }
}
