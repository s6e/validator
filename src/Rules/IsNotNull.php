<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsNotNull extends BaseRule
{
    public function __construct(?string $path, string $type = "notNullRequired", string $message = "Not null Value required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return ($value !== null);
    }
}
