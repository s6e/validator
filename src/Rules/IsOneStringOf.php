<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsOneStringOf extends BaseRule
{
    private array $list;

    public function __construct(?string $path, array $list, string $type = "oneStringOfRequired", string $message = "One string of %s is required.")
    {
        parent::__construct($path, $type, $message, $list);
        $this->list = $list;
    }

    public function check($value): bool
    {
        return is_string($value) && in_array($value, $this->list, true);
    }
}
