<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsJson extends BaseRule
{
    public function __construct(?string $path, string $type = "jsonRequired", string $message = "Json required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return is_string($value) && !empty($value) && filter_var($value, FILTER_VALIDATE_URL);
    }
}
