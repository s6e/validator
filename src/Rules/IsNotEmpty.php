<?php

namespace S6e\Validator\Rules;

use S6e\Validator\BaseRule;

class IsNotEmpty extends BaseRule
{
    public function __construct(?string $path, string $type = "notEmptyRequired", string $message = "Not empty value required.")
    {
        parent::__construct($path, $type, $message);
    }

    public function check($value): bool
    {
        return !empty($value);
    }
}
