<?php

namespace S6e\Validator;

class ValidationResult
{
    private Messages $messages;

    public function __construct()
    {
        $this->messages = new Messages();
    }

    public function addMessage(Message $message): self {
        $this->messages->append($message);

        return $this;
    }

    public function getMessages(): Messages {
        return $this->messages;
    }

    public function isValid(): bool {
        return $this->messages->count() == 0;
    }
}
