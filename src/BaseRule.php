<?php

namespace S6e\Validator;

abstract class BaseRule implements RuleInterface {
    private string $message;
    private string $path;
    private array $params;
    private string $type;

    /**
     * @param string $message
     * @param ?string $path
     */
    public function __construct(?string $path, string $type, string $message, ...$params)
    {
        $this->message = $message;
        $this->path = $path;
        $this->params = $params;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMessage(): string {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    public function getMessageParams(): array {
        return array_map(function($element) {
            if (is_array($element)) {
                return implode(", ", $element);
            } else {
                return (string)$element;
            }
        }, $this->params);
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }
}
