<?php

namespace S6e\Validator;

class RuleOptions
{
    const CONTINUE = 0;
    const MODE_SKIP_SUB_PATHS = 1;
    const MODE_SKIP_SAME_AND_SUBPATHS = 2;
    const MODE_BREAK = 3;

    private int $mode = self::CONTINUE;

    /**
     * @param int $mode
     * @return RuleOptions
     */
    public function setMode(int $mode): RuleOptions
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }
}
