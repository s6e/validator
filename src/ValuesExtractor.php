<?php

namespace S6e\Validator;

class ValuesExtractor
{
    /**
     * @throws NotAnArrayException
     */
    public static function extract(?string $path, $value): array {
        if ($path == null) {
            return [ new Item(null, $value, true) ];
        }

        if (empty($path)) {
            return [ new Item("", $value, true) ];
        }

        $pathElements = explode(".", $path);
        return self::fetchValues($pathElements, $value);
    }

    /**
     * @throws NotAnArrayException
     */
    private static function fetchValues(array $pathElements, $value, $currentPath = []): array {
        $current = $value;
        $currentExists = true;
        while (count($pathElements) > 0) {
            $element = $pathElements[0];
            array_shift($pathElements);
            $currentPath[] = $element;

            if (!$currentExists) {
                continue;
            }

            if ($element == ":each" || $element == ":keys") {
                if (!is_array($current)) {
                    throw new NotAnArrayException();
                }

                $result = [];
                if ($element == ":keys") {
                    $current = array_keys($current);
                }

                foreach ($current as $key => $item) {
                    $result = array_merge(
                        $result,
                        self::fetchValues($pathElements, $item, array_merge($currentPath, [$key]))
                    );
                }
                return $result;
            }

            if (is_array($current)) {
                if (!array_key_exists($element, $current)) {
                    $currentExists = false;
                    $current = null;
                    continue;
                }

                $current = $current[$element];
            }

            if (is_object($current)) {
                $getter = self::getGetter($element);
                if (!method_exists($current, $getter)) {
                    $currentExists = false;
                    $current = null;
                    continue;
                }

                $current = $current->{$getter}();
            }
        }

        return [new Item(implode(".", $currentPath), $current, $currentExists)];
    }

    private static function getGetter(string $field): string
    {
        return 'get'.self::toCamelCase($field);
    }

    private static function toCamelCase(string $name): string
    {
        $result = "";
        foreach (explode("_", $name) as $element) {
            $result .= ucfirst($element);
        }

        return $result;
    }
}
