<?php

namespace S6e\Validator;

use Countable;
use Iterator;

class RuleSet implements Iterator, Countable
{
    private array $rules = [];
    private int $position = 0;

    public function count(): int
    {
        return count($this->rules);
    }

    public function current(): Rule
    {
        return $this->rules[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->rules[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function get(int $index): ?Rule {
        return $this->rules[$index] ?? null;
    }

    public function append(RuleInterface $rule, ?callable $condition = null, ?RuleOptions $options = null): self {
        $this->rules[] = new Rule(
            $rule,
            $condition,
            $options
        );

        return $this;
    }
}
