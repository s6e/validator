<?php

namespace S6e\Validator;

use Countable;
use Iterator;

class Messages implements Iterator, Countable
{
    private array $messages = [];
    private int $position = 0;

    public function count(): int
    {
        return count($this->messages);
    }

    public function current(): Message
    {
        return $this->messages[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->messages[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function get(int $index): ?Message {
        return $this->messages[$index] ?? null;
    }

    public function append(Message $message): self {
        $this->messages[] = $message;

        return $this;
    }
}
