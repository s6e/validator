<?php

namespace S6e\Validator;

class Item
{
    private ?string $path;
    private mixed $value;
    private bool $pathExists;

    /**
     * @param string|null $path
     * @param mixed $value
     */
    public function __construct(?string $path, mixed $value, bool $pathExists)
    {
        $this->path = $path;
        $this->value = $value;
        $this->pathExists = $pathExists;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isPathExists(): bool
    {
        return $this->pathExists;
    }
}
