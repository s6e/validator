<?php

namespace S6e\Validator;

class Validator
{
    private RuleSet $ruleSet;

    /**
     * @param RuleSet $ruleSet
     */
    public function __construct(RuleSet $ruleSet)
    {
        $this->ruleSet = $ruleSet;
    }

    /**
     * @throws NotAnArrayException
     */
    public function validate($value): ValidationResult {
        $validationResult = new ValidationResult();

        $pathsToSkip = [];
        $subPathsToSkip = [];

        foreach ($this->ruleSet as $rule) {
            $skip = false;
            foreach ($pathsToSkip as $pathToSkip) {
                if ($rule->getRule()->getPath() == $pathToSkip) {
                    $skip = true;
                }
            }

            foreach ($subPathsToSkip as $subPathToSkip) {
                if (str_starts_with($rule->getRule()->getPath().".", $subPathToSkip)) {
                    $skip = true;
                }
            }

            if ($skip) {
                continue;
            }

            $values = ValuesExtractor::extract($rule->getRule()->getPath(), $value);
            foreach ($values as $valueToCheck) {
                if (!$rule->check($valueToCheck)) {
                    $validationResult->addMessage(
                        new Message($valueToCheck->getPath(), $rule->getRule()->getType(), $rule->getRule()->getMessage(), $rule->getRule()->getMessageParams())
                    );

                    if ($rule->getOptions()->getMode() == RuleOptions::MODE_SKIP_SUB_PATHS) {
                        $subPathsToSkip[] = $rule->getRule()->getPath();
                    }

                    if ($rule->getOptions()->getMode() == RuleOptions::MODE_SKIP_SAME_AND_SUBPATHS) {
                        $subPathsToSkip[] = $rule->getRule()->getPath();
                        $pathToSkip[] = $rule->getRule()->getPath();
                    }

                    if ($rule->getOptions()->getMode() == RuleOptions::MODE_BREAK) {
                        return $validationResult;
                    }
                }
            }
        }

        return $validationResult;
    }
}
