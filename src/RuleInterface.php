<?php

namespace S6e\Validator;

interface RuleInterface
{
    public function check($value): bool;
    public function getPath(): ?string;
    public function getType(): string;
    public function getMessage(): string;
    public function getMessageParams(): array;
}
