<?php

namespace S6e\Validator;

class Message {
    private ?string $path;
    private string $message;
    private array $params;
    private string $type;

    /**
     * @param ?string $path
     * @param string $type
     * @param string $message
     * @param array $params
     */
    public function __construct(?string $path, string $type, string $message, array $params)
    {
        $this->path = $path;
        $this->message = $message;
        $this->params = $params;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getMessageParams(): array
    {
        return $this->params;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }
}
