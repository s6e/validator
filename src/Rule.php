<?php

namespace S6e\Validator;

class Rule
{
    private RuleInterface $rule;
    /**
     * @var callable
     */
    private $condition;
    private RuleOptions $options;

    public function __construct(RuleInterface $rule, ?callable $condition = null, ?RuleOptions $options = null)
    {
        $this->rule = $rule;
        if ($condition === null) {
            $this->condition = function() {
                return true;
            };
        } else {
            $this->condition = $condition;
        }
        $this->options = $options ?? new RuleOptions();
    }

    private function getCondition(): callable {
        return $this->condition;
    }

    private function isConditionMet(Item $value): bool {
        return $this->getCondition()($value);
    }

    public function getRule(): RuleInterface
    {
        return $this->rule;
    }

    public function check(Item $value): bool {
        if ($this->isConditionMet($value)) {
            return $this->rule->check($value->getValue());
        }

        return true;
    }

    public function getOptions(): RuleOptions {
        return $this->options;
    }
}
